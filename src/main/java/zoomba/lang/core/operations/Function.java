/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.util.*;

/**
 * The Functional Interface in ZoomBA
 * We do not use Java Functional interfaces because we do not like much types
 */
public interface Function {

    /**
     * The id representing this parameter
     */
    String THIS = "$" ;

    /**
     * The id representing current function in the context
     */
    String ME = "@ME" ;

    /**
     * The id representing the default parameter
     */
    String SWITCH_VALUE = "@$" ;

    /**
     * A class defined to replace null issues in ZoomBA
     * This defines a lack of value in many areas without exception
     */
    final class Nil{
        private Nil(){}

        @Override
        public String toString(){
            return "nil" ;
        }
    }

    /**
     * One single instant of NIL is good enough - singleton
     */
    Nil NIL = new Nil();

    /**
     * One Arg class to Handle the default and named parameters
     * to any functions
     */
    final class NamedArgs extends HashMap<String,Object>{

        /**
         * Create a NamedArgs from a Map
         * @param from map, if it is already NamedArgs, returns that
         * @return a NamedArgs object
         */
        public static NamedArgs fromMap(Map<String,Object> from){
            if ( from instanceof NamedArgs ) return (NamedArgs) from;
            return new NamedArgs(from);
        }

        public NamedArgs(){}

        private NamedArgs(Map<String,Object> from){
            super(from);
        }

        public boolean bool(String name, boolean defaultValue){
            return (boolean)getOrDefault(name,defaultValue);
        }

        public Number number(String name, Number defaultValue){
            return (Number)getOrDefault(name,defaultValue);
        }

        public String string(String name, String defaultValue){
            return getOrDefault(name,defaultValue).toString();
        }

        public Map map(String name, Map defaultValue){
            return (Map)getOrDefault(name, defaultValue);
        }

        public List list(String name, List defaultValue){
            return (List)getOrDefault(name, defaultValue);
        }
    }

    /**
     * A function which is mapper
     * A generic mapper
     */
    interface  Mapper extends Function{

        /**
         * Maps the arguments to an object
         * @param args the variable length arguments
         * @return a single object, the mapped value of the arguments
         */
        Object map(Object...args);
    }

    /**
     * A generic predicate function
     * A generic predicate
     */
    interface  Predicate extends Function{

        /**
         * Given arguments maps to a boolean
         * @param args variable length arguments
         * @return boolean true or false
         */
        boolean accept(Object...args);
    }

    /**
     * An Optional Container defining Maybe Monad
     */
    interface MonadicContainer{

        /**
         * Is the container empty?
         * @return true if it is, false if not
         */
        boolean isNil();

        /**
         * Given the container is non empty returns the value
         * @return either the value or NIL
         */
        Object value();

        /**
         * The JAVA 8 style optional conversion.
         * Optional is not good because, it is a final class
         * Monadic Containers are not class, they are interfaces
         * @return Java 8 Optional
         */
        default Optional<Object> asOptional() {
            return isNil() ? Optional.empty() : Optional.of(value());
        }

    }

    /**
     * A basic implementation of Container
     */
    class MonadicContainerBase implements MonadicContainer {

        /**
         * The actual value of the container object
         */
        public final Object value;

        @Override
        public final boolean isNil(){ return value instanceof Nil ; }

        /**
         * Default constructor - empty container
         */
        public MonadicContainerBase(){
            this( NIL );
        }

        /**
         * Constructor containing an object
         * @param v the object as the value
         */
        public MonadicContainerBase(Object v){
            value = v ;
        }

        @Override
        public Object value() {
            return value;
        }

        @Override
        public boolean equals(Object obj) {
            return  ( obj instanceof MonadicContainer ) &&
               ZTypes.equals ( value, ((MonadicContainer) obj).value() );
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }

        @Override
        public String toString() {
            return String.format("<%s>",this.value);
        }
    }

    /**
     * A container for Success with inner value true
     */
    MonadicContainer SUCCESS = new MonadicContainerBase(true);

    /**
     * A container for Failure with inner value false
     */
    MonadicContainer FAILURE = new MonadicContainerBase(false);

    /**
     * A container for void type with inner value NIL
     */
    MonadicContainer Void = new MonadicContainerBase();

    /**
     * A container for NOTHING same as a reference to Void
     */
    MonadicContainer NOTHING = Void ;

    /**
     * The body of a function, not used as of now
     * @return the body
     */
    String body();

    /**
     * Executes a function with the arguments
     * @param args the variable length arguments
     * @return a container containing the value
     */
    MonadicContainer execute(Object...args);

    /**
     * Name of the function
     * @return the name
     */
    String name();

    /**
     * Defines a NO Operation
     */
    Nop NOP = new Nop();

    /**
     * An identity function
     */
    Identity IDENTITY = new Identity();

    /**
     * Identity for the collectors
     */
    CollectorIdentity COLLECTOR_IDENTITY = new CollectorIdentity();

    /**
     * A predicate that always is true
     */
    TruePredicate TRUE = new TruePredicate();

    /**
     * A predicate that always is false
     */
    FalsePredicate FALSE = new FalsePredicate();

    /**
     * A Comparator function implementation
     */
    final class ComparatorLambda implements Comparator {

        /**
         * The collection object
         */
        public final Object col;

        /**
         * Underlying function instance
         */
        public final Function instance ;

        /**
         * Creates a comparator lambda function from
         * @param col the collection
         * @param f the function
         */
        public  ComparatorLambda(Object col, Function f){
            this.col = col ;
            this.instance = f ;
        }

        /**
         * Java style compareTo(object o1, Object o2) returning an integer -1,0,1
         * @param o the object which is a pair left,right
         * @param index the index
         * @param partial the partial result
         * @return -1,0,1 based on left less than right, left equals right, left is greater than right
         */
        public Number num(Object o, Number index, Object partial){
            Object[] args = new Object[ ]{ index, o , col , partial};
            MonadicContainer mc = instance.execute(args);
            if ( mc instanceof ZException.MonadicException ) throw ((RuntimeException)mc);
            if ( mc.isNil() ) throw new UnsupportedOperationException("Scalar can not return void!");
            Object r = mc.value();
            if ( r instanceof Number ) return ((Number) r);
            return ZNumber.number(mc.value(),0);
        }

        @Override
        public int compare(Object o1, Object o2) {
            Object[] args = new Object[ ]{ -1, new Object[]{ o1, o2 } , col , NIL};
            MonadicContainer mc = instance.execute(args);
            if ( mc.isNil() ) throw new UnsupportedOperationException("Comparator can not return void!");
            Object r = mc.value();
            if ( r instanceof Number ) return ((Number) r).intValue() ;
            boolean lt = ZTypes.bool( r, false );
            if ( lt ) return -1;
            return 1 ;
        }
    }
}

/**
 * Implementation of NOP function
 */
final class Nop implements Function{

    final String body = String.format("def %s(){ }", name() );

    @Override
    public String name() {
        return "_0_" ;
    }


    @Override
    public String body() {
        return body ;
    }

    @Override
    public MonadicContainer execute(Object...args) {
        return Void ;
    }

}

/**
 * Implementation of Identity function
 */
final class Identity implements Function{

    final String body = String.format("def %s(){ @ARGS } ", name() );

    @Override
    public String body() {
        return body ;
    }

    @Override
    public MonadicContainer execute(Object...args) {
        return new MonadicContainerBase(args) ;
    }

    @Override
    public String name() {
        return "_1_" ;
    }

}

/**
 * Implementation of Collector Identity
 */
final class CollectorIdentity implements Function{

    @Override
    public String body() {
        return "return $.o ;";
    }

    @Override
    public MonadicContainer execute(Object... args) {
        return new MonadicContainerBase( args[1] );
    }

    @Override
    public String name() {
        return "CollectorIdentity" ;
    }
}

/**
 * Implementation of an always true Predicate
 */
final class TruePredicate implements Function.Predicate {

    @Override
    public String body() {
        return "def _true_(){ true }";
    }

    @Override
    public Function.MonadicContainer execute(Object...args) {
        return SUCCESS ;
    }

    @Override
    public String name() {
        return "true";
    }

    @Override
    public boolean accept(Object... args) {
        return true;
    }
}

/**
 * Implementation of an always false Predicate
 */
final class FalsePredicate implements Function.Predicate {

    @Override
    public String body() {
        return "def _false_(){ false }";
    }

    @Override
    public Function.MonadicContainer execute(Object...args) {
        return FAILURE ;
    }

    @Override
    public String name() {
        return "false";
    }

    @Override
    public boolean accept(Object... args) {
        return false;
    }

}

