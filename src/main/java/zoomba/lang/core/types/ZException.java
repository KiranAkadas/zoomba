/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.parser.ZoombaNode;
import java.util.HashSet;
import java.util.Set;

/**
 * ZoomBA Error Handling and Assertion Classes
 */
public class ZException extends RuntimeException {

    protected ZException(){
        super("Unknown, No idea!");
    }

    protected ZException(Throwable e){ super(e);}

    protected ZException(String message){ super(message);}

    protected ZException(Throwable e, String message){
        super(message,e);
    }

    /**
     * Tokenizing error
     */
    public static final class Tokenization extends ZException{
        public Tokenization(Throwable e, String message) {
            super(e,message);
        }
    }

    /**
     * Parsing Error
     */
    public static final class Parsing extends ZException{

        /**
         * Error message
         */
        public final String errorMessage;

        /**
         * What are the options which were expected there
         */
        public final Set<String> correctionOptions;

        /**
         * Parsing Exception
         * @param e underlying exception
         * @param message the message
         */
        public Parsing(Throwable e, String message) {
            super(e,message);
            String s = e.getMessage();
            int firstNewLine = s.indexOf("\n");
            errorMessage = s.substring(0,firstNewLine);
            String opts = s.substring( firstNewLine+1);
            firstNewLine = opts.indexOf("\n");
            opts = opts.substring( firstNewLine+1);
            String[] arr = opts.split("\n");
            correctionOptions = new HashSet<>();
            for ( int i = 0 ; i < arr.length; i++ ){
                correctionOptions.add( arr[i].trim().replace("...", "" ) );
            }
        }

        @Override
        public String getMessage(){
            return errorMessage;
        }
    }

    /**
     * Variable Not Found Exception
     */
    public static final class Variable extends ZException{
        public Variable(ZoombaNode node, String name) {
            super( name + " : "  + node.locationInfo());
        }
    }

    /**
     * Arithmetic Execution Exception
     */
    public static final class ArithmeticLogicOperation extends ZException{

        private static String message(Throwable t, ZoombaNode node, String op){
            return String.format("Invalid Arithmetic Logical Operation [%s] : %s --> ( %s )",
                    op, node.locationInfo(), t.getMessage());
        }

        public ArithmeticLogicOperation(ZoombaNode node, Throwable t, String op ) {
            super(t, message(t,node,op) );
        }
    }

    /**
     * Function Execution Exception
     */
    public static final class Function extends ZException{
        public Function(ZoombaNode node, String method ) {
            super( method + " : " + node.locationInfo() );
        }
        public Function(ZoombaNode node, String method, Throwable t ) {
            super(t,  method + " : " + node.locationInfo() );
        }
    }

    /**
     * Property Not Found Exception
     */
    public static final class Property extends ZException{
        public Property(ZoombaNode node, Object targetObject, Object property ) {
            super( String.format("Object{ %s  (%s)} does not have property '%s' of %s", targetObject,
                    targetObject != null ? targetObject.getClass() : "nil" ,
                    property, property == null ? "null" : property.getClass() ) + " : " + node.locationInfo() );
        }

        public Property(ZoombaNode node, Object propertyText ) {
            super( propertyText + " : " + node.locationInfo() );
        }

    }

    /**
     * Import Not Successful Exception
     */
    public static final class Import extends ZException{
        public Import(ZoombaNode node, Object imported ) {
            super( imported + " : " + node.locationInfo() );
        }
    }

    /**
     * The Assertion Infrastructure
     */
    public abstract static class ZRuntimeAssertion extends ZException{

        /**
         * Not an Exception
         */
        public static final ZAssertionException NOT_AN_EXCEPTION = new ZAssertionException();

        /**
         * Returning the cause of the Assertion
         * @return the cause
         */
        public final Object cause(){
           if ( args.length == 0 ) return NOT_AN_EXCEPTION;
           return args[0];
        }

        /**
         * The node which generated the Exception
         */
        public final ZoombaNode here;

        /**
         * The extra arguments which were passed
         */
        public Object[] args;

        /**
         * Creates an Assertion
         * @param n the node who raised it
         * @param o the auxiliary objects
         */
        public ZRuntimeAssertion(ZoombaNode n, Object[] o){
            if ( o == null || o.length == 0 ){

                here = n;
                args = ZArray.EMPTY_ARRAY;
                return;
            }

            args = o;

            Object c = args[0];

            if ( c instanceof CharSequence ){
                args[0] = new ZAssertionException(c.toString());
            } else if ( c instanceof Throwable ){
                args[0] = new ZAssertionException((Throwable)c);
            }
            else {
                args[0] = o;
            }
            here = n ;
        }

        public abstract String myType();

        @Override
        public String getMessage() {
            return ((Throwable)cause()).getMessage();
        }

        @Override
        public String toString() {
            return String.format( "%s --> [ %s ] caused by : %s", myType() , here.locationInfo(), cause() );
        }
    }

    /**
     * The runtime Assertion Panic
     */
    public static class Panic extends ZRuntimeAssertion{

        public Panic(ZoombaNode n, Object[] o) {
            super(n, o);
        }

        @Override
        public String myType() {
            return "Panic";
        }
    }

    /**
     * The runtime Assertion Assert
     */
    public static class Assertion extends ZRuntimeAssertion{

        public Assertion(ZoombaNode n, Object[] o) {
            super(n, o);
        }

        @Override
        public String myType() {
            return "Assertion Failure";
        }
    }

    /**
     * The runtime Exceptions to handle control flow structure
     */
    public static class MonadicException extends ZException implements zoomba.lang.core.operations.Function.MonadicContainer {

        public final Object value;

        public MonadicException(){
            super();
            value = zoomba.lang.core.operations.Function.NIL;
        }

        public MonadicException(Object o){
            super();
            value = o ;
        }

        @Override
        public boolean isNil(){ return value == zoomba.lang.core.operations.Function.NIL; }

        @Override
        public Object value() {
            return value;
        }
    }

    /**
     * The Return Exception
     */
    public static final class Return extends MonadicException{

        public static final Return RETURN = new Return();

        private Return(){ super();}

        public Return(Object o){ super(o);}

    }

    /**
     * The Continue Exception
     */
    public static final class Continue extends MonadicException{

        public static final Continue CONTINUE = new Continue();

        private Continue(){ super();}

        public Continue(Object o){ super(o);}

    }

    /**
     * The Break Exception
     */
    public static final class Break extends MonadicException{

        public static final Break BREAK = new Break();

        private Break(){ super();}

        public Break(Object o){ super(o);}

    }

    /**
     * The Termination Exception
     */
    public static final class ZTerminateException extends ZAssertionException {

        public ZTerminateException(String m){
            super(m);
        }
    }
}

/**
 * Generic Assertion Exception
 */
class ZAssertionException extends ZException{

    public ZAssertionException(){
        super("NO_EXCEPTION");
    }

    public ZAssertionException(Throwable t){
        super(t);
    }

    public ZAssertionException(String m){
        super(m);
    }

}


