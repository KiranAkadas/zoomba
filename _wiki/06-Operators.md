# Operators - Extended

## Arithmetic on Collections

It is obvious that the arithmetic operators should have proper meaning 
for collections. Thus we have:

```js
 col += x // col.add(x) if x is non collection, col.addAll(x) otherwise
 col -= x // col.remove(x) or col.removeAll(x) as above
 col1 * col2 // woha : that is cartesian cross product 
 col1 / col2 // rare, do not use it    
 col1 + x // immutable, works like += generates a new col 
 col1 - x // immutable, works like -= generates a new col 
 col ** power // it is col * col * col ... power times
```

### Arithmetic on Lists and Sets
Let's start with immutable lists :

```js
x = [0,1] // x is immutable 
y = x + 10 // [0,1,10] :: mutable list  
x += 11 // no error, nothing happens at all.
```

For mutable collections :

```js  
x = list(0,1)
x += 11 // x = [0,1,11]  
x -= 0 // x = 1,11
```

Same is applicable for sets :

```js
s = set(0,1,2,3)
s -= 0 // s = { 1, 2, 3 } 
```

And same is applicable for *heap* structures, to mutably add 
elements into the heap. Addition on a heap results a list, not another heap.    

Now, for the cross product for two collections :

```js
b1 = [ true, false ]
b2 = b1 * b1 
//[ @[ true,true ],@[ true,false ],@[ false,true ],@[ false,false ] ] 
b2 = b1 ** 2 // same as above 
```

Now, onto the addAll() behaviour:

```js
x = list(0,1)
y = [2,3]
z = x + y // [ 0,1,2,3 ] 
x += y // [ 0,1,2,3 ] 
```

Just to avoid that, you need to actually tell the system that you 
do not want a flattening behaviour:

```js
x.add( [ 4,5] ) // [ 0,1,2,3,@[ 4,5 ] ]
```

#### Finding Median of a Scalar or Comparable Collection  

One way obviously is to sort it, and then find the median. There is another better alternative, that is to use the heap. Heap followed by the max item 
solves the median problem :

```js
l = list ( [1:n] ) -> { random(100) }
println( l )
len = size(l)
h = heap( len/2 + 1 )
h += l // add all to the heap
println ( h )
median = ( 2 /? len ) ? (h[0] + h[1])/2.0 : h.max
println ( median )
```

To find 90% and stuffs like that, of course we need to use the min heap, instead of using max :

```js
percentile = 0.9
h = heap( floor( len * (1 - percentile) ) + 1 , true )
h += l // add all to the heap
println ( h[0] ) // 90 percentile
```

#### Collection Splicing 

Any integer indexable collection can be spliced. As we already know negative indices are allowed. Thus :

```js
 x = [1,2,3,4,5,6]
 x[-1] // x[ size(x) - 1] :: 6 
 x[-n] // x[ size(x) - n] 
```

In the same way, *col[a:b]* implies splicing on a collection :

```js
 x = [1,2,3,4,5,6]
 x[1:4] // [2,3,4,5] :: both index included   
 x[1:-1] // [2,3,4,5,6] 
```

 Strings are collection of characters, and hence, they shows the same behaviour :

 ```js
 s = "hello" 
 s[0] // _'h'
 s[1:3] // 'ell' as string      
 ```

#### Exponentiating a Collection

There are some idiosyncrasies involved, for example :

```js
(zoomba)l = [0,1,2]
@[ 0,1,2 ] // ZArray
(zoomba) l ** 0
[  ] // ZList
(zoomba) l ** -1
@[ 2,1,0 ] // ZArray
```

Given we are looking at string as a collection of characters, 
same holds for a string too :

```js
 "hello" ** 2 // "hellohello"
 "hello" ** -1 // reverse it : "olleh"
```

### Arithmetic on Dictionary 

Dictionary differs in an interesting way, because a dictionary is a list of 
pair - ( key, value ). Formally :

      dict <-> set of (key, value) 

Thus, addition takes it own meaning:

```js
 d = { 0 : 1, 2 :3 }
 d += [4,5] // add a pair, pair[0] is key, pair[1] is value
 // d = {0=1, 2=3, 4=5} 
```

Now, for the subtraction, the key is the only thing that matters, so :

```js
d -= 0 // {2=3, 4=5} 
```

The special operation division "/" finds all keys where value is the parameter for a dictionary :

```js
d = { 0 : 1, 2 : 0 , 3 : 1 , 4: 1 } 
x =  d / 1 // x = { 0, 3, 4 }  
```

## Set Algebra

The basic set algebra goes along with :

* Union 
* Intersection
* Set Difference : Minus
* Set Symmetric Difference

### Logical Operators for Collections 

    col1 | col2  // Union : Logical OR
    col1 & col2  // Intersection : Logical AND
    col1 - col2  // Difference : nothing new 
    col1 ^ col2  // Symmetric Delta :: ( col1 - col2 ) | ( col2 - col1 )      

### Comparison Operators for Collections

    col1 == col2 // permutation equal 
    col1 != col2 // permutation not equal 
    col1 < col2 // col1 is a pure subcollection of col2
    col1 <= col2 // col1 is a subcollection of col2 or equal to col2
    col1 > col2 // col1 is a pure supercollection of col2
    col1 >= col2 // col1 is a supercollection of col2 or equal to col2


### Set Algebra on Lists and Sets

```js
l1 = list(0,1,1,2,3)
l2 = list(2,1,1,3,0)
l1 == l2 // true 
```

Suppose for an item :
  *left\_count(item)* defines the count on the left list.
  *right\_count(item)* defines the count on the right list.   

The union for a list is defined as:
> the count of the item in the union 
> is given by max(left\_count(item), right\_count(item) )

For the intersection it is defined as :
> the count of the item in the intersection 
> is given by min(left\_count(item), right\_count(item) )

For the difference it is defined as :
> the count of the item in the in the difference 
> is given by max( 0 , left\_count(item) - right\_count(item) )

This axiomatic behaviour is easily generalised to the sets.
Thus:

```js
l1 = [0,1,1,2] 
l2 = [1,2,3,4]
u = l1 | l2 // [0,1,1,2,3,4] 
i = l1 & l2 // [1,2] 
d = l1 ^ l2 // [0,3,4] 
```

#### Mixing Up 

When you mix up types, the left side takes precedence.
That is :

```js
l = [0,1,2,2] 
s = set ( 0,1,2) 
u_l = l | s // [ 0,1,2,2 ]  
u_s = s | l // { 0,1,2 }
```

When you mix, the operation cease become *asymmetric*. 
Symmetric operations are, mathematically where :

    a op b == b op a 

But, as ZoomBA will, in fact :

```js
set(0,1,2) == [0,1,2,2] // false 
```

Thus, when equality is concerned, mixing up container types has repercussions. Thus, these are good rules to go by :

```js
 set(0,1,2) < [0,1,2,2]  // true :: general 
 set(0,1,2) == [0,1,2 ] // true, again  :: they actually are 
```

Remember that. This is conformance in action, ZoomBA treats the types
according to operators, finding the *meaning* of it, from the left side
of the binary operations.


### Set Algebra on Dictionary 

Dictionaries are lists of pairs. Thus, the following logic is observed:

> Intersection : both key and value must much 

> Union : key match, and if value match, then in, else the values are put into a list and put into the same key.

> Difference : Remove the intersection of the dictionary from left. 

> Same way, sub dictionary means precisely subset of entrySet.

> Super Dictionary means super set of entrySet.    

#### Demonstration 

```js
de = {:} // empty dictionary 
d1 = { 0:1 }
d2 = { 2:1 }
d3 = { 3:2 , 2:1 }
    
de < d1 // true, obvious 
d1 == d1 | de // yes, sure 
de == de & d1  // must be 
d3 > d2 // true, makes sense 
```



